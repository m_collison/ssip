/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.example;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URL;
import org.openide.util.Exceptions;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.parsers.KeggOrthologyParser;
import uk.ac.ncl.ssip.parsers.UniProtSwissProt;
//import uk.ac.ncl.ssip.webserver.FileServer;
import uk.ac.ncl.ssip.webserver.SSIPWebServer;

/**
 *
 * @author Matt2
 */
public class ExampleMain {

    public static void main(String[] args) {

        //start web server in new thread
        String fileBase = "D://mni_files/sampleData/";
        (new Thread(new SSIPWebServer(fileBase))).start();

        openWebpage("http://localhost:8080");

        ExampleMain e = new ExampleMain();
        e.doStuff("0.14.2");
    }

    public void doStuff(String version){
        
        String fileRoot = "D://mni_files/sampleData/";//"F://BioSSIP/";//
        String keggFilepath = fileRoot + "ko00001.keg";
        String uniProtFilepath = fileRoot + "uniprot_sprot.dat";
//        String goFilepath = "D://go_daily-termdb.obo-xml";
        String graphFile = fileRoot + "results/gephi_export_all_" + version + ".gexf";
        Neo4J backend = new Neo4J(fileRoot + "neo4j-databases/" + version, fileRoot + "results/performance-" + version);

        //connect to database 
        backend.initialiseDatabaseConnection();

        //create index
        backend.createIndex();

        KeggOrthologyParser keggParser = new KeggOrthologyParser(keggFilepath, backend);

        keggParser.parseFile();
        
        UniProtSwissProt uniProtParser = new UniProtSwissProt(uniProtFilepath, backend);
        
        uniProtParser.parseFile();
        
        //query database
        System.out.println("export all code starts here...");
        GephiExporter gexfExportSubgraph = new GephiExporter();

        gexfExportSubgraph.export(backend.returnAllNodesMap(), graphFile);

        //close database again 
        backend.finaliseDatabaseConnection();
    }
    public static void openWebpage(String urlString) {
        try {
            Desktop.getDesktop().browse(new URL(urlString).toURI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
