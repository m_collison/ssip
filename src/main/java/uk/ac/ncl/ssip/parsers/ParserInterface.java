/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.ncl.ssip.parsers;

import uk.ac.ncl.ssip.dataaccesslayer.GraphHandlerInterface;

/**
 *
 * @author Matt2
 */
public interface ParserInterface {
    
    public void parseFile();
    public void setHandler(GraphHandlerInterface handler);
    public void setFilepath(String handler);
}
