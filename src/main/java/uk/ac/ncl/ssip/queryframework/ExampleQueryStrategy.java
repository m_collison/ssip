/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.queryframework;

import java.util.Map;
import uk.ac.ncl.ssip.dataaccesslayer.Neo4J;
import uk.ac.ncl.ssip.dataaccesslayer.StepDescription;
import uk.ac.ncl.ssip.export.GephiExporter;
import uk.ac.ncl.ssip.metadata.MetaDataInterface;
import uk.ac.ncl.ssip.metadata.SSIPNode;

/**
 *
 * @author Matt2
 */
public class ExampleQueryStrategy {

    public static void main(String[] args) {

        String version = "0.1.3.1";
        String fileRoot = "F://BioSSIP/";//"D://mni_files/sampleData/";
        String graphFile = fileRoot + "results/gephi_export_" + version + "_subgraph_K16197.gexf";
        Neo4J backend = new Neo4J(fileRoot + "neo4j-databases/" + version, fileRoot + "results/performance-" + version + ".txt");

        backend.initialiseDatabaseConnection();
        backend.syncIndexes(10);

        StepDescription step = new StepDescription(2, true);

        Map<String, MetaDataInterface> subgraph2 = backend.traversal(
                new SSIPNode("KOlevel4", "K16197"), step);
        GephiExporter gexfExportSubgraph2 = new GephiExporter();
        gexfExportSubgraph2.export(subgraph2, graphFile);

//        Map<String, MetaDataInterface> subgraph = backend.traversal(
//                new SSIPNode("UniProt", "Q7U1B9"), step);
//        GephiExporter gexfExportSubgraph = new GephiExporter();
//        gexfExportSubgraph.export(subgraph, "Q7U1B9_" + graphFile);
//
//        subgraph = backend.traversal(
//                new SSIPNode("UniProt", "Q6GZX3"), step);
//        gexfExportSubgraph = new GephiExporter();
//        gexfExportSubgraph.export(subgraph, "Q6GZX3_" + graphFile);

        //close database again 
        backend.finaliseDatabaseConnection();
    }
}
