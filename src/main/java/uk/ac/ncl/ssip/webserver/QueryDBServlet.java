/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.ssip.webserver;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author matt
 */
public class QueryDBServlet extends HttpServlet  {
        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                resultsWebPage(response);
    }
    
        private void resultsWebPage(HttpServletResponse response) {
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);

        try {
            Scanner sc = new Scanner(new File("./public_html/query.html"));
            while (sc.hasNextLine()) {
                String str = sc.nextLine();

                if (str.contains("<!--insert pipeline information-->")) {
                    System.getProperties().list(response.getWriter());
                }
                
                if (str.contains("<!--insert performance json data-->")){
                    Scanner sc2 = new Scanner(new File("./public_html/results/performance.json"));
                    while(sc2.hasNextLine()){
                        response.getWriter().println(sc2.nextLine());
                    }
                }

                if (str.contains("Untitled.gexf")) {
                    str = str.replace("Untitled.gexf", "./results/gephi_export_all.gexf");
                }
                response.getWriter().println(str);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
