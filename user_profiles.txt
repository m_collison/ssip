Application developers from scratch:
Create a java maven project
include ssip-core dependency in pom
  -include dependency for ssip-core
  -implement AbstractMain
  -include AbstractMain in jar build
  -include project information for deploy
develop parsers and query framework against metadata API
  -code against interfaces and metadata
deploy pre-configured system for publication
  -setup public IP port forwarding
  -maven deploy

Live users:-
Query user:
Write and modify queries on integrated dataset

Build user:
Build modified/updated integrated datasets and run pre-compiled analyses [within reason]

Independent users:-
User:
Easily get source code and recreate dataset remotely
Write and modify queries on integrated dataset
Build modified/updated integrated datasets and run pre-compiled analyses [within reason]

Phase 2 developer:
Create maven java project
include ssip-core dependency in pom
include ssip-extension dependencies in pom
develop parsers and query framework against metadata API
deploy pre-configured system for publication
